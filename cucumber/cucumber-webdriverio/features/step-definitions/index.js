//Complete siguiendo las instrucciones del taller
var {defineSupportCode} = require('cucumber');
var {expect} = require('chai');

defineSupportCode(({Given, When, Then}) => {
  Given('I go to losestudiantes home screen', () => {
    browser.url('/');
    if(browser.isVisible('button=Cerrar')) {
      browser.click('button=Cerrar');
    }
  });

  When('I open the login screen', () => {
    browser.waitForVisible('button=Ingresar', 5000);
    browser.click('button=Ingresar');
  });

  When('I fill a wrong email and password', () => {
    var cajaLogIn = browser.element('.cajaLogIn');

    var mailInput = cajaLogIn.element('input[name="correo"]');
    mailInput.click();
    mailInput.keys('wrongemail@example.com');

    var passwordInput = cajaLogIn.element('input[name="password"]');
    passwordInput.click();
    passwordInput.keys('123467891')
  });

  When('I try to login', () => {
    var cajaLogIn = browser.element('.cajaLogIn');
    cajaLogIn.element('button=Ingresar').click()
  });

  Then('I expect to not be able to login', () => {
    browser.waitForVisible('.aviso.alert.alert-danger', 5000);
  });

  When(/^I fill with (.*) and (.*)$/ , (correo, password) => {
    var cajaLogIn = browser.element('.cajaLogIn');

    cajaLogIn.element('input[name="correo"]').waitForVisible(5000);
    var mailInput = cajaLogIn.element('input[name="correo"]');
    mailInput.click();
    mailInput.keys(correo);

    var passwordInput = cajaLogIn.element('input[name="password"]');
    passwordInput.click();
    passwordInput.keys(password)
  });

  Then('I expect to see {string}', error => {
    browser.waitForVisible('.aviso.alert.alert-danger', 5000);
    var alertText = browser.element('.aviso.alert.alert-danger').getText();
    expect(alertText).to.include(error);
  });

   Then('I expect to see ', () => {
    browser.waitForExist('ul[role="menu"]', 5000);
    var cuenta = browser.element('ul[role="menu"]');
    var cuentaText = cuenta.element('a[role="menuitem"]').getHTML();
    expect(cuentaText).to.include("Salir");
  });
 
  When(/^I fill registry with (.*) and (.*) and (.*) and (.*) and (.*)$/ , (nombre, apellido, correo, password, acepta) => {
    var cajaSignUp = browser.element('.cajaSignUp');

    cajaSignUp.element('input[name="nombre"]').waitForVisible(5000);
    var elemento = cajaSignUp.element('input[name="nombre"]');
    elemento.click();
    elemento.keys(nombre);

    elemento = cajaSignUp.element('input[name="apellido"]');
    elemento.click();
    elemento.keys(apellido);

    elemento = cajaSignUp.element('input[name="correo"]');
    elemento.click();
    elemento.keys(correo);

    elemento = cajaSignUp.element('select[name="idUniversidad"]');
    elemento.click();
    elemento.element('option[value="universidad-de-los-andes"]').waitForVisible(5000);
    elemento.selectByValue('universidad-de-los-andes');

    elemento = cajaSignUp.element('[type="checkbox"]');
    elemento.click();

    elemento = cajaSignUp.element('select[name="idPrograma"]');
    elemento.click();
    elemento.element('option[value="16"]').waitForVisible(5000);
    elemento.selectByValue('16');

    elemento = cajaSignUp.element('input[name="password"]');
    elemento.click();
    elemento.keys(password)

    if (acepta) {
      elemento = cajaSignUp.element('[type="checkbox"][name="acepta"]');
      elemento.click();
    }

  });

  When('I try to register', () => {
    var cajaSignUp = browser.element('.cajaSignUp');
    cajaSignUp.element('.logInButton').click()
  });

  //Fin
});