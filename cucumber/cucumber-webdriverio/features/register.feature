Feature: Correct Register into losestudiantes
    As an user I want to register myself within losestudiantes website in order to rate teachers correctly

Scenario Outline: Register correct with correct inputs

  Given I go to losestudiantes home screen
    When I open the login screen
    And I fill registry with <nombre> and <apellido> and <correo> and <password> and <acepta>
    And I try to register
    Then I expect to see <error>
    #And

    Examples:
      | nombre | apellido | correo             | password   | acepta | error             |
      | Prueba |          |                    |            |        | Ingresa tu correo |
      | Prueba | Ejemplo  |                    |            |        | Ingresa tu correo |
      | Prueba |          | pa2020@example.com |            |        | Ingresa una contr |
      | Prueba |          |                    | foobar1234 |        | Ingresa tu correo |
      | Prueba |          |                    |            |   1    | Ingresa tu correo |
      |        | Ejemplo  |                    |            |        | Ingresa tu correo |
      | Prueba | Ejemplo  | pa2020@example.com |            |        | Ingresa una contr |
      | Prueba | Ejemplo  |                    | foobar1234 |        | Ingresa tu correo |
      | Prueba | Ejemplo  | pa2020@example.com |            |        | Ingresa una contr |
      | Prueba | Ejemplo  | pa2020@example.com | foobar1234 |        | Debes aceptar los |

      