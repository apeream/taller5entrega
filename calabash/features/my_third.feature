Feature: Opening the help screen

  Scenario: Ver Libraries
    Given I press "Tourism"               
    When I swipe left
    Then I should see "Library Luis angel arango"

  Scenario: Ver rutas estación
    Given I press "Routes"               
    When I swipe left
    And I swipe left
    Then I should see "National Capitol"

  Scenario: Ver rutas estación
    Given I press "Routes"               
    When I swipe left
    And I swipe left
    And I swipe left
    Then I should see "Casa de la Moneda"
